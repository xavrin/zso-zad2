#include "hashdev_headers.h"

static struct pci_device_id pci_ids[] = {
    { PCI_DEVICE(HASHDEV_VENDOR_ID, HASHDEV_DEVICE_ID) },
    { 0, },
};

MODULE_DEVICE_TABLE(pci, pci_ids);

static const u32 H_INIT[H_AMOUNT] = {
    0x67452301, 0xEFCDAB89, 0x98BADCFE, 0x10325476, 0xC3D2E1F0};

static DEFINE_MUTEX(mutex);
static struct class *hashdev_class;
static dev_t first_dev; /* first char device number for this driver */

static int hdev_nrs[DEV_AMOUNT]; /* bitmask for available minor numbers */

/* whether the consumer has already eaten the given position; spinlock required
 * for safety */
static inline int is_processed(struct circ_buf *buf, int pos) {
    return (buf->head < buf->tail) ? (buf->head <= pos && pos < buf->tail)
        : !(buf->tail <= pos && pos < buf->head);
}

/* get the number for the device, it'll serve as a index of minor and part of
 * name of the file in /dev/ */
static int get_hdev_nr(int *nr) {
    int i, ret;
    mutex_lock(&mutex);
    ret = -EBUSY;
    for (i = 0; i < DEV_AMOUNT; i++)
        if (hdev_nrs[i]) {
            hdev_nrs[i] = ret = 0;
            *nr = i;
            break;
        }
    mutex_unlock(&mutex);
    return ret;
}

static void release_hdev_nr(int nr) {
    mutex_lock(&mutex);
    hdev_nrs[nr] = 1;
    mutex_unlock(&mutex);
}

/* getting a buffer for data from a hash_dev device */
static int get_buffer(hash_dev *hdev, buf_t **buf) {
    unsigned long flags;
    if (down_interruptible(&hdev->buf_sem))
        return -ERESTARTSYS;
    spin_lock_irqsave(&hdev->buf_lock, flags);
    *buf = list_first_entry(&hdev->buf_list, buf_t, list);
    list_del(&(*buf)->list);
    spin_unlock_irqrestore(&hdev->buf_lock, flags);
    return 0;
}

static void release_buffer(hash_dev *hdev, buf_t *buf) {
    unsigned long flags;
    spin_lock_irqsave(&hdev->buf_lock, flags);
    list_add(&buf->list, &hdev->buf_list);
    spin_unlock_irqrestore(&hdev->buf_lock, flags);
    up(&hdev->buf_sem);
}

/* updating the tail index of the hash_dev, releasing data buffers, if
 * needed; waking up contexts, if needed; assuming that lists are sorted
 * chronologically */
static void do_update(hash_dev *hdev) {
    int pos;
    unsigned long flags;
    ctx_entry *cur_ctx, *next_ctx;
    buf_entry *cur_buf, *next_buf;

    spin_lock_irqsave(&hdev->cmd_lock, flags);
    pos = (dma_addr_t)ioread32(hdev->io_ptr + HASHDEV_SHA1_CMD_PTR)
        - hdev->cmd_bus_addr;
    hdev->cmd_buf.tail = pos;

    list_for_each_entry_safe(cur_buf, next_buf, &hdev->buf_free_list, list) {
        if (is_processed(&hdev->cmd_buf, cur_buf->pos)) {
            release_buffer(hdev, cur_buf->buf);
            list_del(&cur_buf->list);
            kfree(cur_buf);
        }
        else break;
    }

    list_for_each_entry_safe(cur_ctx, next_ctx, &hdev->ioctl_list, list) {
        if (is_processed(&hdev->cmd_buf, cur_ctx->pos)) {
            cur_ctx->ctx->completed = 1;
            wake_up(&cur_ctx->ctx->queue);
            list_del(&cur_ctx->list);
        }
        else break;
    }

    spin_unlock_irqrestore(&hdev->cmd_lock, flags);
    wake_up(&hdev->cmd_queue); /* wake up process waiting for the cmd_buf */
}

/* handler for 2 interrupts: data fetch and ioctl_complete */
static irqreturn_t hashdev_handler(int irq, void *dev) {
    hash_dev *hdev = dev;
    u32 interrupts,
        flags = HASHDEV_INTR_FETCH_DATA_COMPLETE | IOCTL_COMPLETE_INTR;
    interrupts = ioread32(hdev->io_ptr + HASHDEV_INTR);
    if (!(interrupts & flags))
        return IRQ_NONE;
    iowrite32(interrupts & flags, hdev->io_ptr + HASHDEV_INTR);
    do_update(hdev);
    return IRQ_HANDLED;
}

static void init_context(context *ctx) {
    int i;
    for (i = 0; i < H_AMOUNT; i++)
        ctx->h[i] = cpu_to_be32(H_INIT[i]);
    ctx->count = ctx->size = ctx->completed = 0;
}

/* writes to the device count commands from the cmds structure;
 * needs a semaphore on a device, it takes care of the jump;
 * bentry - entry for the buffer to release, centry - entry for the context to
 * wake up, data_ind - index of the command after which data buffer can be
 * released */
static void __write_commands(hash_dev *hdev, u32 *cmds, size_t count,
        buf_entry *bentry, ctx_entry *centry, int data_ind) {
    struct circ_buf *cbuf = &hdev->cmd_buf;
    unsigned long flags;
    int head, i, ioctl_pos, data_pos;

    head = cbuf->head;

    for (i = 0; i < count; i++) {
        if (head + 4 == CMD_BUF_SIZE)
            head = 0; /* writing the jump command */
        *(uint32_t*)(cbuf->buf + head) = cmds[i];
        if (i == data_ind)
            data_pos = head;
        if (i == count-1)
            ioctl_pos = head;
        head += 4;
    }

    spin_lock_irqsave(&hdev->cmd_lock, flags);
    cbuf->head = head;
    if (bentry) {
        bentry->pos = data_pos;
        list_add_tail(&bentry->list, &hdev->buf_free_list);
    }
    if (centry) {
        centry->pos = ioctl_pos;
        list_add_tail(&centry->list, &hdev->ioctl_list);
    }
    iowrite32(hdev->cmd_bus_addr + cbuf->head,
              hdev->io_ptr + HASHDEV_SHA1_CMD_END);
    spin_unlock_irqrestore(&hdev->cmd_lock, flags);
}

static inline int is_space_available(struct circ_buf *cbuf, size_t count) {
    /* we need an extra position of space for the possible jump command */
    return CIRC_SPACE(cbuf->head, cbuf->tail, CMD_BUF_SIZE) >= (count + 1) * 4;
}

static void write_commands(hash_dev *hdev, u32 *cmds, size_t count,
        buf_entry *bentry, ctx_entry *centry, int data_ind) {
    down(&hdev->sem);
    wait_event(hdev->cmd_queue, is_space_available(&hdev->cmd_buf, count));
    __write_commands(hdev, cmds, count, bentry, centry, data_ind);
    up(&hdev->sem);
}

static int write_commands_interruptible(hash_dev *hdev, u32 *cmds,
        size_t count, buf_entry *bentry, ctx_entry *centry, int data_ind) {
    if (down_interruptible(&hdev->sem))
        return -ERESTARTSYS;
    if (wait_event_interruptible(hdev->cmd_queue,
            is_space_available(&hdev->cmd_buf, count))) {
        up(&hdev->sem);
        return -ERESTARTSYS;
    }
    __write_commands(hdev, cmds, count, bentry, centry, data_ind);
    up(&hdev->sem);
    return 0;
}

static int hashdev_open(struct inode *inode, struct file *filp) {
    context *ctx;

    if (!(ctx = kmalloc(sizeof *ctx, GFP_KERNEL)))
        return -ENOMEM;
    ctx->hdev = container_of(inode->i_cdev, hash_dev, cdev);
    if (!(ctx->h = dma_pool_alloc(ctx->hdev->pool, GFP_KERNEL,
                                  &ctx->h_bus_addr))) {
        kfree(ctx);
        return -ENOMEM;
    }
    mutex_init(&ctx->mutex);
    init_waitqueue_head(&ctx->queue);
    init_context(ctx);
    filp->private_data = ctx;
    return 0;
}

static int hashdev_release(struct inode *inode, struct file *filp) {
    context *ctx = filp->private_data;
    u32 cmd;
    ctx_entry centry;
    hash_dev *hdev = ctx->hdev;
    centry.ctx = ctx;
    /* we have to be sure that device doesn't touch our dma memory anymore */
    cmd = HASHDEV_CMD_SHA1_INTR(IOCTL_IDX);
    write_commands(hdev, &cmd, 1, NULL, &centry, 1);
    wait_event(ctx->queue, ctx->completed);
    dma_pool_free(ctx->hdev->pool, ctx->h, ctx->h_bus_addr);
    kfree(ctx);
    return 0;
}

static ssize_t hashdev_write(struct file *filp, const char __user *ubuf,
                             size_t count, loff_t *offp) {
    context *ctx = filp->private_data;
    size_t remaining = count + ctx->count;
    size_t written = 0, hmany, dma_count, count_backup;
    hash_dev *hdev = ctx->hdev;
    buf_t *buf;
    buf_entry *bentry;
    u32 cmds[MAX_CMD_LEN];
    int cmd_amount, data_pos, err;

    if (mutex_lock_interruptible(&ctx->mutex))
        return -ERESTARTSYS;
    /* the total amount of data we have */
    remaining = count + ctx->count;
    while (remaining / H_BLOCK_SIZE) {
        count_backup = ctx->count;
        dma_count = 0;
        if (get_buffer(hdev, &buf)) {
            err = -ERESTARTSYS;
            goto error;
        }
        if (ctx->count) {
            memcpy(buf->kaddr + dma_count, ctx->buffer, ctx->count);
            dma_count += ctx->count;
            remaining -= ctx->count;
            ctx->count = 0;
        }
        hmany = min((size_t)BUF_SIZE, rounddown(remaining + dma_count,
                    (size_t)H_BLOCK_SIZE)) - dma_count;
        if (copy_from_user(buf->kaddr + dma_count, ubuf + written, hmany)) {
            err = -EFAULT;
            goto buf_release;
        }
        dma_count += hmany;

        if (!(bentry = kmalloc(sizeof *bentry, GFP_KERNEL))) {
            err = -ENOMEM;
            goto buf_release;
        }
        bentry->buf = buf;

        cmd_amount = 0;
        cmds[cmd_amount++] = HASHDEV_CMD_SHA1_H_LOAD(ctx->h_bus_addr);
        cmds[cmd_amount++] = HASHDEV_CMD_SHA1_DATA_PTR(buf->bus_addr);
        cmds[cmd_amount++] = HASHDEV_CMD_SHA1_DATA_COUNT(dma_count);
        data_pos = cmd_amount;
        cmds[cmd_amount++] = HASHDEV_CMD_SHA1_H_SAVE(ctx->h_bus_addr);

        if ((err = write_commands_interruptible(hdev, cmds, cmd_amount, bentry,
                                                NULL, data_pos)))
            goto bentry_free;

        ctx->size += dma_count * 8;
        written += hmany;
        remaining -= hmany;
    }
    if (remaining) {
        if (copy_from_user(ctx->buffer + ctx->count, ubuf + written,
                           count - written)) {
            err = -EFAULT;
            goto error;
        }
        ctx->count += (count - written);
        written = count;
    }

out:
    mutex_unlock(&ctx->mutex);
    *offp += written;
    return written;

bentry_free:
    kfree(bentry);
buf_release:
    release_buffer(hdev, buf);
    ctx->count = count_backup;
error:
    if (written)
        goto out;
    mutex_unlock(&ctx->mutex);
    return err;
}

static long hashdev_ioctl(struct file *filp, unsigned int cmd,
                          unsigned long arg) {
    struct hashdev_ioctl_get_result __user *ures, res;
    context *ctx = filp->private_data;
    hash_dev *hdev = ctx->hdev;
    int i, err, cmd_amount = 0, data_pos;
    size_t dma_count;
    ssize_t zeros;
    buf_t *buf;
    u32 cmds[MAX_CMD_LEN], hle[H_AMOUNT];
    buf_entry *bentry;
    ctx_entry centry;

    if (mutex_lock_interruptible(&ctx->mutex))
        return -ERESTARTSYS;

    if (cmd != HASHDEV_IOCTL_GET_RESULT) {
        err = -ENOTTY;
        goto error;
    }

    ures = (struct hashdev_ioctl_get_result*)arg;
    if ((err = get_buffer(hdev, &buf)))
        goto error;
    memcpy(buf->kaddr, ctx->buffer, ctx->count);
    dma_count = ctx->count;
    buf->kaddr[dma_count++] = 0x80;
    zeros = H_BLOCK_SIZE - sizeof(ctx->size) - dma_count;
    if (zeros < 0)
        zeros += H_BLOCK_SIZE;
    memset(buf->kaddr + dma_count, 0, zeros);
    dma_count += zeros;
    *(u64*)(buf->kaddr + dma_count) = cpu_to_be64(ctx->size + ctx->count * 8);
    dma_count += sizeof(ctx->size);

    if (!(bentry = kmalloc(sizeof *bentry, GFP_KERNEL))) {
        err = -ENOMEM;
        goto buf_release;
    }
    bentry->buf = buf;
    centry.ctx = ctx;

    cmds[cmd_amount++] = HASHDEV_CMD_SHA1_H_LOAD(ctx->h_bus_addr);
    cmds[cmd_amount++] = HASHDEV_CMD_SHA1_DATA_PTR(buf->bus_addr);
    cmds[cmd_amount++] = HASHDEV_CMD_SHA1_DATA_COUNT(dma_count);
    data_pos = cmd_amount;
    cmds[cmd_amount++] = HASHDEV_CMD_SHA1_H_SAVE(ctx->h_bus_addr);
    cmds[cmd_amount++] = HASHDEV_CMD_SHA1_INTR(IOCTL_IDX);

    if ((err = write_commands_interruptible(hdev, cmds, cmd_amount, bentry,
                                            &centry, data_pos)))
        goto bentry_free;

    /* unfortunately, we can't be interrupted below */
    wait_event(ctx->queue, ctx->completed);

    for (i = 0; i < H_AMOUNT; i++)
        hle[i] = be32_to_cpu(ctx->h[i]);
    for (i = 0; i < H_AMOUNT * 4; i++)
        res.hash[i] = *((u8*)hle + i);
    if (copy_to_user(ures, &res, sizeof res)) {
        err = -EFAULT;
        goto error;
    }
    init_context(ctx);
    mutex_unlock(&ctx->mutex);
    return 0;

bentry_free:
    kfree(bentry);
buf_release:
    release_buffer(hdev, buf);
error:
    mutex_unlock(&ctx->mutex);
    return err;
}

static struct file_operations fops = {
    .owner = THIS_MODULE,
    .open = hashdev_open,
    .release = hashdev_release,
    .write = hashdev_write,
    .unlocked_ioctl = hashdev_ioctl,
    .compat_ioctl = hashdev_ioctl
};

static int probe(struct pci_dev *pdev, const struct pci_device_id *id) {
    hash_dev *hdev;
    int err, i;
    buf_t *buf, *pos, *n;
    dev_t devno;

    if (!(hdev = kmalloc(sizeof *hdev, GFP_KERNEL)))
        return -ENOMEM;
    pci_set_drvdata(pdev, hdev);
    sema_init(&hdev->sem, 1);
    sema_init(&hdev->buf_sem, BUF_AMOUNT);
    spin_lock_init(&hdev->buf_lock);
    spin_lock_init(&hdev->cmd_lock);
    INIT_LIST_HEAD(&hdev->buf_list);
    INIT_LIST_HEAD(&hdev->buf_free_list);
    INIT_LIST_HEAD(&hdev->ioctl_list);
    init_waitqueue_head(&hdev->cmd_queue);

    if ((err = pci_enable_device(pdev)))
        goto KFREE_HDEV;
    if ((err = pci_request_regions(pdev, DRV_NAME)))
        goto PCI_DEVICE;
    if (!(hdev->io_ptr = pci_iomap(pdev, 0, BAR_SIZE))) {
        err = -ENOMEM;
        goto PCI_REGIONS;
    }

    pci_set_master(pdev);
    if ((err = pci_set_dma_mask(pdev, DMA_BIT_MASK(32))))
        goto IOMAP;
    if ((err = pci_set_consistent_dma_mask(pdev, DMA_BIT_MASK(32))))
        goto IOMAP;

    for (i = 0; i < BUF_AMOUNT; i++) {
        if (!(buf = kmalloc(sizeof *buf, GFP_KERNEL))) {
            err = -ENOMEM;
            goto DMA_SPACE;
        }
        if (!(buf->kaddr = dma_alloc_coherent(&pdev->dev, BUF_SIZE,
                &buf->bus_addr, GFP_KERNEL))) {
            kfree(buf);
            err = -ENOMEM;
            goto DMA_SPACE;
        }
        list_add(&buf->list, &hdev->buf_list);
    }
    if (!(hdev->cmd_buf.buf = dma_alloc_coherent(&pdev->dev, CMD_BUF_SIZE,
            &hdev->cmd_bus_addr, GFP_KERNEL))) {
        err = -ENOMEM;
        goto DMA_SPACE;
    }
    hdev->cmd_buf.head = hdev->cmd_buf.tail = 0; /* empty buffer */
    /* writing the jump command at the end, once for all */
    *(u32*)(hdev->cmd_buf.buf + CMD_BUF_SIZE - 4) =
        HASHDEV_CMD_SHA1_JUMP(hdev->cmd_bus_addr);

    if (!(hdev->pool = dma_pool_create(DRV_NAME, &pdev->dev, POOL_SIZE,
                                       POOL_ALIGN, 0))) {
        err = -ENOMEM;
        goto DMA_CMD;
    }

    /* clearing interrupts */
    iowrite32(ALL_INTERRUPTS, hdev->io_ptr + HASHDEV_INTR);

    if ((err = request_irq(pdev->irq, &hashdev_handler, IRQF_SHARED,
                           DRV_NAME, hdev)))
        goto DMA_POOL;

    /* enabling interrupts */
    iowrite32(HASHDEV_INTR_FETCH_DATA_COMPLETE | IOCTL_COMPLETE_INTR,
              hdev->io_ptr + HASHDEV_INTR_ENABLE);
    iowrite32(0, hdev->io_ptr + HASHDEV_SHA1_DATA_COUNT);
    iowrite32(hdev->cmd_bus_addr, hdev->io_ptr + HASHDEV_SHA1_CMD_PTR);
    iowrite32(hdev->cmd_bus_addr, hdev->io_ptr + HASHDEV_SHA1_CMD_END);

    /* enabling the data and command blocks */
    iowrite32(HASHDEV_ENABLE_FETCH_DATA | HASHDEV_ENABLE_FETCH_CMD,
              hdev->io_ptr + HASHDEV_ENABLE);

    /* now setting up the char device */
    if ((err = get_hdev_nr(&hdev->nr)))
        goto FREE_IRQ;

    cdev_init(&hdev->cdev, &fops);
    hdev->cdev.owner = THIS_MODULE;
    devno = first_dev + hdev->nr;
    if ((err = cdev_add(&hdev->cdev, devno, 1)))
        goto RELEASE_HDEV;

    hdev->dev = device_create(hashdev_class, &pdev->dev, devno, NULL,
                              "%s%d", DEV_NAME, hdev->nr);
    if (IS_ERR(hdev->dev)) {
        err = PTR_ERR(hdev->dev);
        goto CDEV_DEL;
    }

    printk(KERN_INFO "registering device nr %d: on major %d minor %d\n",
           hdev->nr, MAJOR(devno), MINOR(devno));
    return 0;

CDEV_DEL:
    cdev_del(&hdev->cdev);
RELEASE_HDEV:
    release_hdev_nr(hdev->nr);
FREE_IRQ:
    free_irq(pdev->irq, hdev);
    iowrite32(0, hdev->io_ptr + HASHDEV_INTR_ENABLE);
    iowrite32(0, hdev->io_ptr + HASHDEV_ENABLE);
DMA_POOL:
    dma_pool_destroy(hdev->pool);
DMA_CMD:
    dma_free_coherent(&pdev->dev, CMD_BUF_SIZE, hdev->cmd_buf.buf,
                      hdev->cmd_bus_addr);
DMA_SPACE:
    list_for_each_entry_safe(pos, n, &hdev->buf_list, list) {
        dma_free_coherent(&pdev->dev, BUF_SIZE, pos->kaddr,
                        pos->bus_addr);
        list_del(&pos->list);
        kfree(pos);
    }
IOMAP:
    pci_iounmap(pdev, hdev->io_ptr);
PCI_REGIONS:
    pci_release_regions(pdev);
PCI_DEVICE:
    pci_disable_device(pdev);
KFREE_HDEV:
    kfree(hdev);
    printk(KERN_WARNING "registering unsuccessful\n");
    return err;
}

static void remove(struct pci_dev *pdev) {
    buf_t *pos, *n;
    hash_dev *hdev = pci_get_drvdata(pdev);

    device_destroy(hashdev_class, first_dev + hdev->nr);
    cdev_del(&hdev->cdev);
    release_hdev_nr(hdev->nr);
    free_irq(pdev->irq, hdev);
    iowrite32(0, hdev->io_ptr + HASHDEV_INTR_ENABLE);
    iowrite32(0, hdev->io_ptr + HASHDEV_ENABLE);
    dma_pool_destroy(hdev->pool);
    dma_free_coherent(&pdev->dev, CMD_BUF_SIZE, hdev->cmd_buf.buf,
                      hdev->cmd_bus_addr);
    list_for_each_entry_safe(pos, n, &hdev->buf_list, list) {
        dma_free_coherent(&pdev->dev, BUF_SIZE, pos->kaddr,
                        pos->bus_addr);
        list_del(&pos->list);
        kfree(pos);
    }
    pci_iounmap(pdev, hdev->io_ptr);
    pci_release_regions(pdev);
    pci_disable_device(pdev);
    kfree(hdev);
}

static struct pci_driver hashdev_pci = {
    .name = DRV_NAME,
    .id_table = pci_ids,
    .probe = probe,
    .remove = remove,
};

static __init int hashdev_init(void) {
    int err, i;

    hashdev_class = class_create(THIS_MODULE, DRV_NAME);
    if (IS_ERR(hashdev_class))
        return PTR_ERR(hashdev_class);
    if ((err = alloc_chrdev_region(&first_dev, 0, DEV_AMOUNT, DRV_NAME)))
        goto class_destroy;
    for (i = 0; i < DEV_AMOUNT; i++)
        hdev_nrs[i] = 1;
    if ((err = pci_register_driver(&hashdev_pci)))
        goto unregister_region;
    return 0;

unregister_region:
    unregister_chrdev_region(first_dev, DEV_AMOUNT);
class_destroy:
    class_destroy(hashdev_class);
    return err;
}

static __exit void hashdev_exit(void) {
    pci_unregister_driver(&hashdev_pci);
    unregister_chrdev_region(first_dev, DEV_AMOUNT);
    class_destroy(hashdev_class);
}

module_init(hashdev_init);
module_exit(hashdev_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Dawid Łazarczyk");
