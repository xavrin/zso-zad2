#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include "hashdev_ioctl.h"

#define TEST_NUM 4

char* tests[] = {
"fjksadfjslkdfjsadlkfhklsjdfhksjdfhklasjdfhlksjadfhklsadjfhklsdjhfkjdhfjdskalfhaskjldfhlaskjdfhlskajdfhklasjdfhklasjdfhksjdfhksajdfhlaskdjfhaskldjfhaslkdjfhalskdjfhaklsjdfhalksjdfhlaksjdfhksjadfhaklsjdfhklasjdfhaskjdfhaskdjlfhsakjldfhaskljdfhskadjfhaskljdfhslakdjfhasdjklfhasdjklfhaskjdlfhsakdjfhaskljdfh",
"fjksadfjslkdfjsadlkfhklsjdfhksjdfhklasjdfhlksjadfhklsadjfhklsdjhfkjdhfjdskalfhaskjldfhlaskjdfhlskajdfhklasjdfhklasjdfhksjdfhksajdfhlaskdjfhaskldjfhaslkdjfhalskdjfhaklsjdfhalksjdfhlaksjdfhksjadfhaklsjdfhklasjdfhaskjdfhaskdjlfhsakjldfhaskljdfhskadjfhaskljdfhslakdjfhasdjklfhasdjklfhaskjdlfhsakdjfhaskljdfhdfgsdfgdfgggggggggggggggggggggggffffffffffffffffffffgggggggggu",
"56756yh56vuy67u67cvuv76gj7y87bvj87b878b6g7876tyhjjy8j78j78j7j78j78j5j786k8,8o,lk,kl.kl.kl,kl.........................,,,,,,,,,,,,,,,,,,u,kuikuyik",
"f384fr3jh948fj394ifj3984fj398j3894f34f34f.3.4f,.34,f/34.3.3/4.f34;f3'4;3'4f;34'f;34f34]f[34]f[3]4[f3]4[f]34[f]34f3k4oru48rthdfjsdjkvndkfjfhoiw3uerfoijoi2u342iu34oi1i11oi423i4uo23i4uo2i3j4io23jfklenfkejrfhieruhi3urhfi3u4hfi3u"
};

char str_ans[100];

char* answers[] = {
"80DD79B4 2BE2FCB5 B5225B53 BB1F8451 7628EF34",
"5DE3AE8A F1F47AA6 D0E46F8 ADB1C4A0 5616B0DA",
"E6DC637 36CB1453 4128D66A D02AA3A2 816BE105",
"F748167B 2E4E3A2C A7EAABE8 6D95C259 CD68CE9B"
};



#define PATH0 "/dev/hash0"

int main()
{
    int fds[TEST_NUM], err, i, j;
    struct hashdev_ioctl_get_result res;
    uint32_t *ans;

    for (i = 0; i < TEST_NUM; i++)
    if ((fds[i] = open(PATH0, O_WRONLY)) == -1) {
        fprintf(stderr, "ERROR open\n");
        return -1;
    }

    for (i = 0; i < TEST_NUM; i++) {
        if ((err = write(fds[i], tests[i], strlen(tests[i]))) == -1) {
            fprintf(stderr, "ERROR write\n");
            return -1;
        }
    }
    for (i = 0; i < TEST_NUM; i++) {
        printf("\nTest %d\n", i);
        if ((err = ioctl(fds[i], HASHDEV_IOCTL_GET_RESULT, &res)) == -1) {
            fprintf(stderr, "ERROR ioctl\n");
            return -1;
        }
        printf("\t");
        ans = (uint32_t*)res.hash;
        sprintf(str_ans, "%X %X %X %X %X", ans[0], ans[1], ans[2], ans[3], ans[4]);
        if (strcmp(str_ans, answers[i]) == 0)
            printf("Passed\n");
        else
            fprintf(stderr, "answer=\n%s\n,should=\n%s\n", str_ans, answers[i]);
    }
    for (i = 0; i < TEST_NUM; i++)
        close(fds[i]);
    return 0;
}
