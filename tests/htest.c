#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include "hashdev_ioctl.h"

#define TESTB_1 "abcdbcdecdefdefgefghfghighij"
#define TESTB_2 "hijkijkljklmklmnlmnomnopnopq"

#define TESTC_PART1 (1000*1000 - 10000)

#define CBUFF 1000*1000

#define TEST_NUM 5

char* tests[] = {
    "abc",
    TESTB_1 TESTB_2,
    "litwo,ojczyzno moja ty jestes jak zdrowie, ile cie trzeba cenic ten ",
    "",
    "",
};

int repeat[] = {
    0, 0, 0, 67, 1000*1000
};

char str_ans[100];

char* answers[] = {
    "A9993E36 4706816A BA3E2571 7850C26C 9CD0D89D",
    "84983E44 1C3BD26E BAAE4AA1 F95129E5 E54670F1",
    "65EB6E5 7FA34947 D2074883 A24AC067 479D0470",
    "C70CC62A 2ECB15F4 BF1B7090 4DD62137 3D79F311",
    "34AA973C D4C4DAA4 F61EEB2B DBAD2731 6534016F",
};


char buff[CBUFF];

#define PATH0 "/dev/hash0"

int main()
{
    int fd, err, i, j;
    struct hashdev_ioctl_get_result res;
    uint32_t *ans;

    if ((fd = open(PATH0, O_WRONLY)) == -1)
        return -1;

    for (i = 0; i < TEST_NUM; i++) {
        printf("\nTest %d\n", i);

        if (repeat[i] > 0) {
            memset(buff, 'a', repeat[i]);
            if (i == 4) {
                if ((err = write(fd, buff, TESTC_PART1)) == -1) {
                    fprintf(stderr, "ERROR write\n");
                    return -1;
                }
                for (j = 0; j < repeat[i] - TESTC_PART1; j++) {
                    if ((err = write(fd, buff, 1)) == -1) {
                        fprintf(stderr, "ERROR write\n");
                        return -1;
                    }
                }
            }
            else {
                for (j = 0; j < repeat[i]; j++) {
                    if ((err = write(fd, buff, 1)) == -1) {
                        fprintf(stderr, "ERROR write\n");
                        return -1;
                    }
                }
            }

        }
        else {
            if ((err = write(fd, tests[i], strlen(tests[i]))) == -1) {
                fprintf(stderr, "ERROR write\n");
                return -1;
            }
        }
        if ((err = ioctl(fd, HASHDEV_IOCTL_GET_RESULT, &res)) == -1) {
            fprintf(stderr, "ERROR ioctl\n");
            return -1;
        }
        printf("\t");
        ans = (uint32_t*)res.hash;
        sprintf(str_ans, "%X %X %X %X %X", ans[0], ans[1], ans[2], ans[3], ans[4]);
        if (strcmp(str_ans, answers[i]) == 0)
            printf("Passed\n");
        else
            printf("Failed\n");
    }

    close(fd);
    return 0;
}
