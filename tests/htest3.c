#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <pthread.h>
#include "hashdev_ioctl.h"

#define TEST_NUM 4

char* tests[] = {
    "fjksadfjslkdfjsadlkfhklsjdfhksjdfhklasjdfhlksjadfhklsadjfhklsdjhfkjdhfjdskalfhaskjldfhlaskjdfhlskajdfhklasjdfhklasjdfhksjdfhksajdfhlaskdjfhaskldjfhaslkdjfhalskdjfhaklsjdfhalksjdfhlaksjdfhksjadfhaklsjdfhklasjdfhaskjdfhaskdjlfhsakjldfhaskljdfhskadjfhaskljdfhslakdjfhasdjklfhasdjklfhaskjdlfhsakdjfhaskljdfh",
    "fjksadfjslkdfjsadlkfhklsjdfhksjdfhklasjdfhlksjadfhklsadjfhklsdjhfkjdhfjdskalfhaskjldfhlaskjdfhlskajdfhklasjdfhklasjdfhksjdfhksajdfhlaskdjfhaskldjfhaslkdjfhalskdjfhaklsjdfhalksjdfhlaksjdfhksjadfhaklsjdfhklasjdfhaskjdfhaskdjlfhsakjldfhaskljdfhskadjfhaskljdfhslakdjfhasdjklfhasdjklfhaskjdlfhsakdjfhaskljdfhdfgsdfgdfgggggggggggggggggggggggffffffffffffffffffffgggggggggu",
    "56756yh56vuy67u67cvuv76gj7y87bvj87b878b6g7876tyhjjy8j78j78j7j78j78j5j786k8,8o,lk,kl.kl.kl,kl.........................,,,,,,,,,,,,,,,,,,u,kuikuyik",
    "f384fr3jh948fj394ifj3984fj398j3894f34f34f.3.4f,.34,f/34.3.3/4.f34;f3'4;3'4f;34'f;34f34]f[34]f[3]4[f3]4[f]34[f]34f3k4oru48rthdfjsdjkvndkfjfhoiw3uerfoijoi2u342iu34oi1i11oi423i4uo23i4uo2i3j4io23jfklenfkejrfhieruhi3urhfi3u4hfi3u"
};

char* answers[] = {
    "80DD79B4 2BE2FCB5 B5225B53 BB1F8451 7628EF34",
    "5DE3AE8A F1F47AA6 D0E46F8 ADB1C4A0 5616B0DA",
    "E6DC637 36CB1453 4128D66A D02AA3A2 816BE105",
    "F748167B 2E4E3A2C A7EAABE8 6D95C259 CD68CE9B"
};

#define PATH0 "/dev/hash0"
#define THREADS_NUM (TEST_NUM * 50)
#define BUFF_SIZE 100

int result[THREADS_NUM] = {0, };

void* worker_thread(void *data) {
    char str_ans[BUFF_SIZE];
    int nr = *(int*)data, test_nr = nr % TEST_NUM;
    int fd, err, i;
    struct hashdev_ioctl_get_result res;
    uint32_t *ans;

    free(data);

    if ((fd = open(PATH0, O_WRONLY)) == -1) {
        fprintf(stderr, "ERROR open\n");
        return (void*)-1;
    }

    if ((err = write(fd, tests[test_nr], strlen(tests[test_nr]))) == -1) {
        fprintf(stderr, "ERROR write\n");
        return (void*)-1;
    }
    if ((err = ioctl(fd, HASHDEV_IOCTL_GET_RESULT, &res)) == -1) {
        fprintf(stderr, "ERROR ioctl\n");
        return (void*)-1;
    }
    ans = (uint32_t*)res.hash;
    sprintf(str_ans, "%X %X %X %X %X", ans[0], ans[1], ans[2], ans[3], ans[4]);
    if (strcmp(str_ans, answers[test_nr]) == 0)
        result[nr] = 1;
    else
        fprintf(stderr, "answer=\n%s\n,should=\n%s\n", str_ans, answers[i]);
    close(fd);
    return 0;
}

int main()
{
    int i, *ind_ptr, ok = 1;
    pthread_attr_t attr;
    pthread_t threads[THREADS_NUM];
    if (pthread_attr_init(&attr)) {
        fprintf(stderr, "ERROR attr_init");
        return -1;
    }
    if (pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE)) {
        fprintf(stderr, "ERROR detach");
        return -1;
    }
    for (i = 0; i < THREADS_NUM; i++) {
        if (!(ind_ptr = malloc(sizeof *ind_ptr))) {
            fprintf(stderr, "ERROR malloc");
            return -1;
        }
        *ind_ptr = i;
        if (pthread_create(&threads[i], &attr, worker_thread, ind_ptr)) {
            fprintf(stderr, "ERROR pthread_create");
            return -1;
        }
    }
    for (i = 0; i < THREADS_NUM; i++)
        if (pthread_join(threads[i], NULL)) {
            fprintf(stderr, "ERROR pthread_join");
            return -1;
        }
    for (i = 0; i < THREADS_NUM; i++)
        if (!result[i])
            ok = 0;
    if (ok) {
        printf("TEST passed\n");
        return 0;
    }
    else {
        printf("TEST failed\n");
        return -1;
    }
}
