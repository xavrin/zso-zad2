#ifndef HASHDEV_HEADERS_H
#define HASHDEV_HEADERS_H

#include <linux/pci.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/semaphore.h>
#include <linux/init.h>
#include <linux/dma-mapping.h>
#include <linux/interrupt.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <linux/circ_buf.h>
#include <asm/uaccess.h>
#include <linux/spinlock.h>
#include <linux/dmapool.h>

#include "hashdev.h"
#include "hashdev_ioctl.h"

#define DRV_NAME "hashdev_driver"
#define DEV_NAME "hash"
#define BAR_SIZE (4 * 1024)
#define H_AMOUNT 5
#define H_BLOCK_SIZE HASHDEV_SHA1_BLOCK_SIZE
#define ALL_INTERRUPTS ((1 << 18) - 1)
/* later in code we'll assume that BUF_SIZE is at least twice as large as
 * the sha1 block size and is devisible by the H_BLOCK_SIZE */
#define BUF_SIZE (1 << 12)
/* command buffer has to have a size being a power of two! */
#define CMD_BUF_SIZE (1 << 12)
#define BUF_AMOUNT 32 /* amount of data buffers per device */
#define DEV_AMOUNT 256 /* maximum amount of devices at the same time */
#define IOCTL_IDX 0 /* id of a custom interrupt for the completion */
#define IOCTL_COMPLETE_INTR HASHDEV_INTR_USER(IOCTL_IDX)
#define MAX_CMD_LEN (10) /* maximum size of cmds table */
#define POOL_SIZE (H_AMOUNT * 4) /* size of a pool for context's h values */
#define POOL_ALIGN 4 /* align of the pool */

/* structure for the dma data buffer that the hash device reads the data
 * from */
typedef struct buf_t {
    char *kaddr; /* kernel virtual address of the dma area */
    dma_addr_t bus_addr; /* bus address of the dma area */
    struct list_head list;
} buf_t;

/* structure for the hash device */
typedef struct hash_dev {
    struct cdev cdev; /* char device structure */
    int nr; /* number of the device */
    struct device *dev; /* sys device structure */
    struct semaphore sem; /* semaphore for mutual exclusion */
    void __iomem *io_ptr; /* pointer to I/O memory of device */
    struct dma_pool *pool; /* pool for contexts' h values */

    struct semaphore buf_sem; /* semaphore for the buffers, it'll serve as a
                                 way of checking, if there are free buffers */
    spinlock_t buf_lock; /* spinlock for buf_list */
    struct list_head buf_list; /* list of free dma buffers of the device */

    spinlock_t cmd_lock; /* lock for the cmd_buf and the lists below */
    struct list_head ioctl_list; /* contexts waiting for ioctl complete */
    struct list_head buf_free_list; /* buffers to be freed at some point */

    wait_queue_head_t cmd_queue; /* queue for wait for the command buffer */
    struct circ_buf cmd_buf; /* the command buffer */
    dma_addr_t cmd_bus_addr; /* bus address of the command buffer */
} hash_dev;

/* Context of using a hashdev device, that is temporary values
 * h[i] and buffer for collecting data from user */
typedef struct context {
    u32 *h; /* dma memory for h values */
    dma_addr_t h_bus_addr; /* bus address of h values */
    char buffer[H_BLOCK_SIZE]; /* buffer for temporary data */
    size_t count; /* number of bytes written in the current buffer */
    u64 size; /* total size of written bytes */
    hash_dev *hdev; /* pointer to the device */
    struct mutex mutex;

    int completed; /* information whether the ioctl completed */
    wait_queue_head_t queue; /* queue for ioctl waits */
} context;

/* structure to hold contexts in lists */
typedef struct ctx_entry {
    context *ctx;
    int pos; /* index of byte of the end of this contexts's commands */
    struct list_head list;
} ctx_entry;

/* structure for the buffers to release */
typedef struct buf_entry {
    buf_t *buf;
    int pos;
    struct list_head list;
} buf_entry;

#endif
